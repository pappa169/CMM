# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(509, 300)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.listView = QtWidgets.QListView(self.centralWidget)
        self.listView.setGeometry(QtCore.QRect(20, 30, 371, 251))
        self.listView.setObjectName("listView")
        self.label = QtWidgets.QLabel(self.centralWidget)
        self.label.setGeometry(QtCore.QRect(20, 10, 101, 16))
        self.label.setObjectName("label")
        self.copyButton = QtWidgets.QPushButton(self.centralWidget)
        self.copyButton.setGeometry(QtCore.QRect(400, 30, 101, 51))
        self.copyButton.setObjectName("copyButton")
        self.mpdButton = QtWidgets.QPushButton(self.centralWidget)
        self.mpdButton.setGeometry(QtCore.QRect(400, 90, 101, 51))
        self.mpdButton.setObjectName("mpdButton")
        MainWindow.setCentralWidget(self.centralWidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Car Music Manager"))
        self.label.setText(_translate("MainWindow", "Available Stuff:"))
        self.copyButton.setText(_translate("MainWindow", "Copy"))
        self.mpdButton.setText(_translate("MainWindow", "Setup MPD"))
