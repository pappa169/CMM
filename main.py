import sys, os, shutil, stat
from PyQt5 import QtCore, QtGui, QtWidgets
from main_window import Ui_MainWindow
from mympd import MpdWidget
from config import Config
 
class MyMainWindow(QtWidgets.QMainWindow):
	DEST_DIR = "/mnt/Music/"
	def __init__(self, parent=None):
		super(MyMainWindow, self).__init__(parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.ui.mpdButton.clicked.connect(self.startMPD)
		self.ui.copyButton.clicked.connect(self.copy)
	def displayMusic(self, path):
		self.__musicPath__ = path
		model = QtGui.QStandardItemModel(self.ui.listView)
		if path is not None:
			for dir in os.listdir(path):
				if os.path.isdir(path+dir) and dir[0] != '.' :
					item = QtGui.QStandardItem(dir)
					item.setCheckable(True)
					model.appendRow(item)
		self.ui.listView.setModel(model)
	def startMPD(self):
		mpdWidget = MpdWidget(self)
		mpdWidget.show()
	def copy(self):
		model = self.ui.listView.model()
		for row in range(model.rowCount()):
			if model.item(row).checkState() == QtCore.Qt.Checked:				
				self.__copy__(self.__musicPath__ + model.item(row).text(), Config.LIBRARY_DIR + '/' + model.item(row).text())

	def __copy__(self, src,dest):
		try:
			if os.path.exists(dest):
				shutil.rmtree(dest)
			shutil.copytree(src, dest)
			#set read permissions to everybody
			for root, dir, files in os.walk(dest):
				for file in files:
					fullpath = os.path.join(root, file)
					os.chmod(fullpath , os.stat(fullpath).st_mode | stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH )
			QtWidgets.QMessageBox.information(self, src, "OK")
		except shutil.Error as e:
			err = ("%s") % e
			QtWidgets.QMessageBox.critical(self, src, err)
		except OSError as e:
			err = ("%s") % e
			QtWidgets.QMessageBox.critical(self, src, err)
			
if __name__ == "__main__":
	musicPath = None
	if len(sys.argv) > 1:
		musicPath = sys.argv[1]
	#print("Path: " + str(musicPath))
	app = QtWidgets.QApplication(sys.argv)
	myMainWindow = MyMainWindow()
	myMainWindow.show()
	myMainWindow.displayMusic(musicPath)
	sys.exit(app.exec_())
