from PyQt5 import QtCore, QtGui, QtWidgets
from mpd_window import Ui_Mpd
from mpd import MPDClient,CommandError
import shutil,os,re
from config import Config


class MyMpdInfo():
    def __init__(self):
        self.client = MPDClient()  # create client object
        self.client.connect("localhost", 6600) # connect to localhost:6600

    def close(self):
        self.client.close()  # send the close command
        self.client.disconnect()  # disconnect from the server

    def readDatabase(self):
        for dir in self.client.listfiles():
            print(dir['directory'])
            pass;

class MpdWidget(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MpdWidget, self).__init__(parent)
        self.ui = Ui_Mpd()
        self.ui.setupUi(self)
        self.__mpdInfo__ = MyMpdInfo()
        self.readDBase()
        self.readPlaylist()
        self.ui.PlaylistDeleteButton.clicked.connect(self.deleteSelectedSongPlaylist)
        self.ui.ClearAllButton.clicked.connect(self.ClearAllPlaylist)
        self.ui.AddButton.clicked.connect(self.addFolderToPlaylist)
        self.ui.AddAllButton.clicked.connect(self.addAllToPlaylist)
        self.ui.mpdUpdateButton.clicked.connect(self.mpdUpdate)
        self.ui.PlayButton.clicked.connect(self.mpdPlay)
        self.ui.StopButton.clicked.connect(self.mpdStop)
        self.ui.NextButton.clicked.connect(self.mpdNext)
        self.ui.DeleteFromCollectionButton.clicked.connect(self.deleteFromCollection)
        checked = QtCore.Qt.Checked if('1' == self.__mpdInfo__.client.status()['random']) else QtCore.Qt.Unchecked
        self.ui.RandomBox.setChecked(checked)
        self.ui.RandomBox.stateChanged.connect(self.Random)
        self.updateMountBar()

    def readDBase(self):
        model = QtGui.QStandardItemModel(self.ui.DBaseListView)
        for musicItem in self.__mpdInfo__.client.listfiles():
            if 'directory' in musicItem:
                item = QtGui.QStandardItem(musicItem['directory'])
                model.appendRow(item)
            elif 'file' in musicItem:
                item = QtGui.QStandardItem(musicItem['file'])
                model.appendRow(item)
            model.sort(0)
        self.ui.DBaseListView.setModel(model)

    def addAllToPlaylist(self):
        for musicItem in self.__mpdInfo__.client.listfiles():
            print(dir)
            if 'directory' in musicItem:
                if not musicItem['directory'].startswith('.'):
                    try:
                        self.__mpdInfo__.client.add(musicItem['directory'])
                    except CommandError:
                        print("szar van!")
            elif 'file' in musicItem:
                print(musicItem['file'])
                try:
                    self.__mpdInfo__.client.add(musicItem['file'])
                except CommandError:
                    print("szar van!")
        self.readPlaylist()

    def readPlaylist(self):
        model = QtGui.QStandardItemModel(self.ui.PlaylistListView)
        for title in self.__mpdInfo__.client.playlistinfo():
            if('artist' in title and 'title' in title):
                item = QtGui.QStandardItem(title['artist'] + ' - ' + title['title'])
            elif ('title' in title):
                item = QtGui.QStandardItem(title['title'])
            model.appendRow(item)
        self.ui.PlaylistListView.setModel(model)
        self.ui.PlaylistListView.setCurrentIndex(model.index(0,0))

    def deleteSelectedSongPlaylist(self):
        row = self.ui.PlaylistListView.currentIndex().row()
        if row != -1:
            self.__mpdInfo__.client.delete(row)
        self.readPlaylist()
        model = self.ui.PlaylistListView.model()
        self.ui.PlaylistListView.setCurrentIndex(model.index(row, 0))

    def ClearAllPlaylist(self):
        self.__mpdInfo__.client.clear()
        self.readPlaylist()

    def addFolderToPlaylist(self):
        if self.ui.DBaseListView.currentIndex().data() != None:
            self.__mpdInfo__.client.add(self.ui.DBaseListView.currentIndex().data())
            self.readPlaylist()

    def mpdUpdate(self):
        self.__mpdInfo__.client.update()
        self.readDBase()

    def mpdPlay(self):
        row = self.ui.PlaylistListView.currentIndex().row()
        if row != -1:
            self.__mpdInfo__.client.play(row)
        else:
            self.__mpdInfo__.client.play()

    def mpdStop(self):
        self.__mpdInfo__.client.stop()

    def mpdNext(self):
        self.__mpdInfo__.client.next()

    def Random(self):
        random = '1' if  self.ui.RandomBox.isChecked() else '0'
        self.__mpdInfo__.client.random(random)

    def closeEvent(self, event):
        self.__mpdInfo__.close()

    def deleteFromCollection(self):
        if self.ui.DBaseListView.currentIndex().data() != None:
            if QtWidgets.QMessageBox.question(self, "Remove", "Are you sure to remove " + self.ui.DBaseListView.currentIndex().data() + " ?",
                                           QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.Yes) == QtWidgets.QMessageBox.Yes:
                shutil.rmtree(Config.LIBRARY_DIR + "/" + self.ui.DBaseListView.currentIndex().data())
                self.mpdUpdate()
        self.updateMountBar()

    def updateMountBar(self):
        p = os.popen('df -h ' + Config.MNT_POINT)
        self.ui.MountBar.setValue(int(re.split(r'\s{1,}', p.readlines()[1].strip())[4][:-1]))
